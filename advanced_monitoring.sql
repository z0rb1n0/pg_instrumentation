BEGIN TRANSACTION;
SET TRANSACTION READ WRITE;

CREATE OR REPLACE PROCEDURAL LANGUAGE 'plpython3u';

CREATE EXTENSION IF NOT EXISTS "hstore";

DROP SCHEMA IF EXISTS administration CASCADE;
DROP SCHEMA IF EXISTS instrumentation CASCADE;
DROP SCHEMA IF EXISTS linux CASCADE;
DROP SCHEMA IF EXISTS utilities CASCADE;

CREATE SCHEMA utilities;
COMMENT ON SCHEMA utilities IS 'Utility functions to support all other instrumentation schemas. Some of them overlap functionality in the legendary "adminpack", but we cannot assume that that is installed';
CREATE SCHEMA linux;
COMMENT ON SCHEMA linux IS 'Abstraction of the Linux operating system monitoring interfaces. Largely implemented by abstracting /proc contents as views. Hence it''s "Linux" and not "POSIX" or "Unix"';
CREATE SCHEMA instrumentation;
COMMENT ON SCHEMA instrumentation IS 'Entities that overlay information from the OS to Postgres''s monitoring (eg: join pg_stat_activity with /proc information to get the best of both worlds when it comes to session monitoring)';
CREATE SCHEMA administration;
COMMENT ON SCHEMA "administration" IS 'State-changing features. They can go as far as deliberately crippling the installation as part of a STONITH event. Use your judgement';



CREATE FUNCTION utilities.schema_version() RETURNS INT LANGUAGE 'sql' IMMUTABLE SECURITY INVOKER CALLED ON NULL INPUT COST 1000000000 AS $ver$
	SELECT -1;
$ver$;
GRANT EXECUTE ON FUNCTION utilities.schema_version() TO PUBLIC;
COMMENT ON FUNCTION utilities.schema_version() IS 'Static schema version indicator (same numbering scheme as postgres'' server_version_num)
This is the closest thing to a constant that we can define at this level
It intended to be set to a real value by the run-time with a CREATE OR REPLACE whenever the node initialization
code determines that the housekeeping schema has to be rebuilt to the one coming with the current run-time version';




CREATE FUNCTION utilities.ls_dir_abs(
	dir_name TEXT,
	soft_fail BOOLEAN = false
) RETURNS SETOF TEXT LANGUAGE 'plpython3u' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
import os
try:
	return(os.listdir(dir_name))
except:
	if (soft_fail):
		return []
	else:
		raise
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.ls_dir_abs(TEXT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.ls_dir_abs(TEXT, BOOLEAN) IS 'Insecure version of pg_ls_dir. This allows the listing of any directory in the file system the backend process has access to.
The second argument allows one to specify if soft fail is desired (empty list returned on failures)';


CREATE FUNCTION utilities.read_binary_files_abs(
	file_names TEXT[],
	read_start BIGINT DEFAULT 0,
	read_length BIGINT DEFAULT 67108864,
	soft_fail BOOLEAN = false
) RETURNS TABLE (
	fn TEXT,
	fd BYTEA
) LANGUAGE 'plpython3u' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$

	# we keep track of what files we already returned
	seen_files = []
	for next_fn in file_names:
		if (next_fn not in seen_files):
			seen_files += next_fn
			next_fh = None
			try:
				next_fh = open(next_fn, "rb")

				if (read_start > 0):
					os.lseek(next_fh, read_start, os.SEEK_SET)

				yield {"fn": next_fn, "fd": next_fh.read(read_length)}
				next_fh.close()

			except:
				if (next_fh is not None):
					next_fh.close()

				if (soft_fail):
					yield {"fn": next_fn, "fd": None}
				else:
					raise

	return

$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.read_binary_files_abs(TEXT[], BIGINT, BIGINT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.read_binary_files_abs(TEXT[], BIGINT, BIGINT, BOOLEAN) IS 'Multi-file version of read_binary_file_abs (see below). Allows the passing of multiple file names as an array.
This exists for optimization reasons: plpythonu starts a new interpreter for each function call, which makes read_binary_file_abs inefficient for a large number of files.
The fourth argument (soft fail) handles errors during retrieval by returning NULL in the fd column for the failed file
Returns a row for each input file. Output columns:
	fn (file name): the full path of the file
	fd (file data): data retrieved for the specified file. Null
';



CREATE FUNCTION utilities.read_binary_file_abs(
	file_name TEXT,
	read_start BIGINT DEFAULT 0,
	read_length BIGINT DEFAULT 67108864,
	soft_fail BOOLEAN = false
) RETURNS BYTEA LANGUAGE 'sql' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
	SELECT
		rbfa.fd
	FROM
		utilities.read_binary_files_abs(ARRAY[$1], $2, $3, soft_fail) AS rbfa
	;
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.read_binary_file_abs(TEXT, BIGINT, BIGINT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.read_binary_file_abs(TEXT, BIGINT, BIGINT, BOOLEAN) IS 'Insecure version of pg_read_binary_file. This allows reading of any file the backend process has access to.
Actually implemented as a single-file wrapper for read_binary_files_abs().
The fourth argument (soft fail) causes null to be returned in case of read failure
';

CREATE FUNCTION utilities.read_files_abs(
	file_names TEXT[],
	read_start BIGINT DEFAULT 0,
	read_length BIGINT DEFAULT 67108864,
	soft_fail BOOLEAN = false
) RETURNS TABLE (
	fn TEXT,
	fd TEXT
) LANGUAGE 'sql' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
	SELECT
		fn,
		convert_from(rbfa.fd, (SELECT s.setting FROM pg_settings AS s WHERE (s.name = 'client_encoding')))
	FROM
		utilities.read_binary_files_abs($1, $2, $3, soft_fail) AS rbfa
	;
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.read_files_abs(TEXT[], BIGINT, BIGINT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.read_binary_files_abs(TEXT[], BIGINT, BIGINT, BOOLEAN) IS 'Multi-file version of read_file_abs (see below). Allows the passing of multiple file names as an array.
Same result set as read_binary_files_abs, with the exception that fd is an encoding-safe text column (in fact, it is just a wrapper for it)
The fourth argument (soft fail) handles errors during retrieval by returning NULL in the fd column for the failed file

This function is rather inefficient and should not be used for large files as it does not directly open the underlying files
in text mode but simply converts the data in memory.
';


CREATE FUNCTION utilities.read_file_abs(
	file_name TEXT,
	read_start BIGINT DEFAULT 0,
	read_length BIGINT DEFAULT 67108864,
	soft_fail BOOLEAN = false
) RETURNS TEXT LANGUAGE 'sql' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
	SELECT
		rbfa.fd
	FROM
		utilities.read_files_abs(ARRAY[$1], $2, $3, soft_fail) AS rbfa
	;
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.read_file_abs(TEXT, BIGINT, BIGINT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.read_file_abs(TEXT, BIGINT, BIGINT, BOOLEAN) IS 'Insecure version of pg_read_file. This allows reading of any file the backend process has access to.
Actually implemented as a single-file wrapper for read_files_abs()
The fourth argument (soft fail) causes null to be returned in case of read failure
';


CREATE FUNCTION utilities.write_binary_file_abs(
	file_name TEXT,
	file_data BYTEA,
	append_mode BOOLEAN = false
) RETURNS BIGINT LANGUAGE 'plpython3u' SECURITY INVOKER VOLATILE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
import os

wfd = open(file_name, ("a" if (append_mode) else "w"))
wb = os.write(wfd.fileno(), file_data)
wfd.close()

return wb

$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.write_binary_file_abs(TEXT, BYTEA, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.write_binary_file_abs(TEXT, BYTEA, BOOLEAN) IS 'VERY dangerous function. Allows one to write files anywhere the postmaster process owner can
(therefore potentially wreaking havoc in the cluster files). 
Another reason this is bad is that it allows non-transactional changes to happen from within a transaction. Use it wisely: don''t in your application queries.
Given that any failure at this level is fatal, 
Does not have a soft fail mode
';


CREATE FUNCTION utilities.write_file_abs(
	file_name TEXT,
	file_data TEXT,
	append_mode BOOLEAN = false
) RETURNS BIGINT LANGUAGE 'sql' SECURITY INVOKER VOLATILE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
	SELECT utilities.write_binary_file_abs(
		file_name,
		convert_to(file_data, (SELECT s.setting FROM pg_settings AS s WHERE (s.name = 'client_encoding')))
	);
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION utilities.write_file_abs(TEXT, TEXT, BOOLEAN) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION utilities.write_file_abs(TEXT, TEXT, BOOLEAN) IS 'TEXT-friendly wrapper for utilities.write_binary_file_abs()
';


CREATE TYPE "utilities"."command_result" AS (
	"status" INT,
	"stdout" BYTEA,
	"stderr" BYTEA
);
COMMENT ON TYPE "utilities"."command_result" IS 'The output of a shell_exec() call. The resulting exit state and streams are returned as a tuple';


CREATE FUNCTION "utilities"."shell_exec"(
	cmd_line TEXT,
	stdin_data BYTEA = NULL
) RETURNS "utilities"."command_result" LANGUAGE 'plpython3u' SECURITY INVOKER STABLE CALLED ON NULL INPUT COST 1000000000 AS $CODE$
import subprocess

cmd_bufs = ["", ""]
try:
	cmd_p = subprocess.Popen(
		cmd_line,
		stdin = (subprocess.PIPE if (stdin_data is not None) else None),
		stdout = subprocess.PIPE,
		stderr = subprocess.PIPE,
		shell = True
	)
	cmd_bufs = cmd_p.communicate(stdin_data)
	cmd_p.stdout.close()
	cmd_p.stderr.close()
	cmd_return = cmd_p.returncode;
except OSError:
	# seems like that the only exception subprocess throws is for "file not found" on exec,
	# so there it goes
	cmd_return = 127
	raise

return {
	"status": int(cmd_return),
	"stdout": cmd_bufs[0],
	"stderr": cmd_bufs[1]
}

$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION "utilities"."shell_exec"(TEXT, BYTEA) FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION "utilities"."shell_exec"(TEXT, BYTEA) IS 'SQL version of system(), with the added benefit of accepting stdin data
Be VERY careful when calling this.

Arguments:
	cmd_line:		the command line tu run. Basically what you''d pass to system()
	stdin:			what to feed to the command standard input. If passed as NULL, stdin is connected to /dev/null
See the comments on the return types for return values';



CREATE FUNCTION linux.get_platform() RETURNS TABLE (
	word_size SMALLINT,
	address_size SMALLINT,
	byte_order TEXT
) LANGUAGE 'plpython3u' IMMUTABLE SECURITY INVOKER CALLED ON NULL INPUT COST 1000000000 AS $platform$
import ctypes
import sys

# note that the byte/bit order names we emit are the
# same as python ones
return ({
		"word_size": 8 * ctypes.sizeof(ctypes.POINTER(ctypes.c_size_t)),
		"address_size": 8 * ctypes.sizeof(ctypes.POINTER(ctypes.c_size_t)),
		"byte_order": sys.byteorder,

	},)
$platform$;
GRANT EXECUTE ON FUNCTION linux.get_platform() TO PUBLIC;
COMMENT ON FUNCTION linux.get_platform() IS 'Retrieves platform-specific information about the system.
Columns names are largely self-explainatory but if any is not that''s specifically documented.
Note that the byte-order string comes from python itself';
CREATE VIEW linux.platform AS
	SELECT * FROM linux.get_platform();
;
COMMENT ON VIEW linux.platform IS 'See invoked function';
GRANT SELECT ON TABLE linux.platform TO PUBLIC;





CREATE FUNCTION linux.get_uname() RETURNS TABLE (
	sysname TEXT,
	nodename TEXT,
	release TEXT,
	version TEXT,
	machine TEXT
) LANGUAGE 'plpython3u' IMMUTABLE SECURITY INVOKER CALLED ON NULL INPUT COST 1000000000 AS $uname$
import os
return (dict(zip(
	("sysname", "nodename", "release", "version", "machine"),
	tuple(os.uname())
)),)
$uname$;
GRANT EXECUTE ON FUNCTION linux.get_uname() TO PUBLIC;
CREATE VIEW linux.uname AS
	SELECT * FROM linux.get_uname();
;
COMMENT ON VIEW linux.uname IS 'See invoked function';
GRANT SELECT ON TABLE linux.uname TO PUBLIC;


CREATE FUNCTION linux.get_sys_cpus() RETURNS TABLE (
	cpu_id SMALLINT,
	physical_package_id SMALLINT,
	core_siblings SMALLINT[],
	core_id SMALLINT,
	thread_siblings SMALLINT[],
	thread_id SMALLINT
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $ctd$
	WITH
		advertised_cpu_dirs AS (
			SELECT
				CAST(REPLACE(scfs.scf_dir, 'cpu', '') AS SMALLINT) AS cpu_id,
				CONCAT('/sys/devices/system/cpu/', scfs.scf_dir, '/topology') AS cpu_topo_dir
			FROM
				utilities.ls_dir_abs('/sys/devices/system/cpu') AS scfs(scf_dir)
			WHERE
				(UPPER(scfs.scf_dir) ~ '^CPU[0-9]{1,14}$')
		)
		SELECT
			ad.cpu_id,
			utilities.read_file_abs(CONCAT(ad.cpu_topo_dir, '/physical_package_id'), 0, 4096, true)::SMALLINT AS physical_package_id,
			-- the following madness explodes lists of cpus specified as ranges and removes duplicates. unfortunately it has to be repeated twice (creating a function for this seems crazy)
			-- we assume that comma-separated fields come in two forms: single cpu id or range (in the hyphen-separated form)
			(
				SELECT
					array_agg(c.cpuid)
				FROM
					(
						SELECT DISTINCT ON ("cpuid")
							CAST((CASE
								WHEN (cfs.cfc ~ '^[0-9]{1,14}$') THEN
									CAST(cfs.cfc AS INT)
								ELSE
									from_range.cfc
							END) AS SMALLINT) AS "cpuid"
						FROM
							UNNEST(CAST(CONCAT('{', utilities.read_file_abs(CONCAT(ad.cpu_topo_dir, '/core_siblings_list'), 0, 16384, true), '}') AS TEXT[])) AS cfs(cfc),
							LATERAL generate_series(
								LEAST(CAST(split_part(cfs.cfc, '-', 1) AS INT), CAST(split_part(cfs.cfc, '-', 2) AS INT)),
								GREATEST(CAST(split_part(cfs.cfc, '-', 1) AS INT), CAST(split_part(cfs.cfc, '-', 2) AS INT)),
								1
							) AS from_range(cfc)
						ORDER BY
							cpuid ASC
					) AS c
			) AS core_siblings,
			CAST(utilities.read_file_abs(CONCAT(ad.cpu_topo_dir, '/core_id'), 0, 4096, true) AS SMALLINT) AS core_id,
			(
				SELECT
					array_agg(c.cpuid)
				FROM
					(
						SELECT DISTINCT ON ("cpuid")
							CAST((CASE
								WHEN (cfs.cfc ~ '^[0-9]{1,14}$') THEN
									CAST(cfs.cfc AS INT)
								ELSE
									from_range.cfc
							END) AS SMALLINT) AS "cpuid"
						FROM
							UNNEST(CAST(CONCAT('{', utilities.read_file_abs(CONCAT(ad.cpu_topo_dir, '/thread_siblings_list'), 0, 16384, true), '}') AS TEXT[])) AS cfs(cfc),
							LATERAL generate_series(
								LEAST(CAST(split_part(cfs.cfc, '-', 1) AS INT), CAST(split_part(cfs.cfc, '-', 2) AS INT)),
								GREATEST(CAST(split_part(cfs.cfc, '-', 1) AS INT), CAST(split_part(cfs.cfc, '-', 2) AS INT)),
								1
							) AS from_range(cfc)
						ORDER BY
							cpuid ASC
					) AS c
			) AS thread_siblings,
			CAST(null AS SMALLINT) AS thread_id
		FROM
			advertised_cpu_dirs AS ad
$ctd$;
COMMENT ON FUNCTION linux.get_sys_cpus() IS '
Implementation of os_sys_cpus. Written as a function to bypass security restrictions.
List of CPUs, as presented in /sys/devices/system/cpu/cpu. The siblings lists are converted into to arrays of smallints. Some trickery (madness?) is used to ensure ordering and uniquess in the arrays.

This function has not been optimized for the new multi-file access functionality yet and is therefore very inefficient on large CPU sets
';

CREATE VIEW linux.sys_cpus AS
	SELECT * FROM linux.get_sys_cpus();
;
COMMENT ON VIEW linux.sys_cpus IS 'See invoked function';
GRANT SELECT ON TABLE linux.sys_cpus TO PUBLIC;


CREATE VIEW linux.sys_cpu_pps AS 
	SELECT DISTINCT ON(pp_id)
		CAST(UPPER(md5(array_to_string(ai.core_siblings, '_'))) AS CHAR(32)) AS pp_id,
		ai.core_siblings AS pp_cores
	FROM
		linux.sys_cpus AS ai
;
COMMENT ON VIEW linux.sys_cpu_pps IS 'List of processor physical packages as presented in /sys/devices/system/cpu/cpu. Advertised id is disregarded and a new one is inferred by grouping by list of cores (whose MD5 hash serves as package "id". there is a chance for collisions but it wouldn''t be a big deal';
GRANT SELECT ON TABLE linux.sys_cpu_pps TO PUBLIC;

CREATE VIEW linux.sys_cpu_cores AS 
	SELECT DISTINCT ON(core_id)
		CAST(UPPER(md5(array_to_string(ai.thread_siblings, '_'))) AS CHAR(32)) AS core_id,
		CAST(UPPER(md5(array_to_string(ai.core_siblings, '_'))) AS CHAR(32)) AS pp_id,
		ai.thread_siblings AS core_threads
	FROM
		linux.sys_cpus AS ai
;
COMMENT ON VIEW linux.sys_cpu_cores IS 'List of processor cores as presented in /sys/devices/system/cpu/cpu. Advertised id is disregarded and a new one is inferred by grouping by list of threads. See linux.sys_cpu_pps for details';
GRANT SELECT ON TABLE linux.sys_cpu_cores TO PUBLIC;

CREATE VIEW linux.sys_cpu_threads AS 
	SELECT
		ai.cpu_id AS thread_id,
		CAST(UPPER(md5(array_to_string(ai.thread_siblings, '_'))) AS CHAR(32)) AS core_id
	FROM
		linux.sys_cpus AS ai
;
COMMENT ON VIEW linux.sys_cpu_threads IS 'List of processor threads as presented in /sys/devices/system/cpu/cpu. No grouping involved but the core ID is inferred by the list of siblings';
GRANT SELECT ON TABLE linux.sys_cpus TO PUBLIC;


CREATE FUNCTION linux.get_proc_meminfo() RETURNS TABLE (
	mem_total BIGINT,
	mem_free BIGINT,
	mem_available BIGINT,
	buffers BIGINT,
	cached BIGINT,
	swap_cached BIGINT,
	active BIGINT,
	inactive BIGINT,
	active_anon BIGINT,
	inactive_anon BIGINT,
	active_file BIGINT,
	inactive_file BIGINT,
	unevictable BIGINT,
	m_locked BIGINT,
	swap_total BIGINT,
	swap_free BIGINT,
	dirty BIGINT,
	writeback BIGINT,
	anon_pages BIGINT,
	mapped BIGINT,
	sh_mem BIGINT,
	slab BIGINT,
	s_reclaimable BIGINT,
	s_unreclaimable BIGINT,
	kernel_stack BIGINT,
	page_tables BIGINT,
	nfs_unstable BIGINT,
	bounce BIGINT,
	writeback_tmp BIGINT,
	commit_limit BIGINT,
	committed_as BIGINT,
	vm_alloc_total BIGINT,
	vm_alloc_used BIGINT,
	vm_alloc_chunk BIGINT,
	hardware_corrupted BIGINT,
	anon_huge_pages BIGINT,
	cma_total BIGINT,
	cma_free BIGINT,
	huge_pages_total BIGINT,
	huge_pages_free BIGINT,
	huge_pages_rsvd BIGINT,
	huge_pages_surp BIGINT,
	huge_pages_size BIGINT,
	direct_map_4k BIGINT,
	direct_map_2m BIGINT
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opm$
	WITH
		crs AS (
			SELECT
				UPPER(split_part((regexp_split_to_array(meminfo.mir, '\s+'))[1], ':', 1)) AS mk,
				((1024 ^ (CASE COALESCE(UPPER((regexp_split_to_array(meminfo.mir, '\s+'))[3]), 'MISSING_UNIT')
					WHEN 'B' THEN 0
					WHEN 'KB' THEN 1
					WHEN 'MB' THEN 2
					WHEN 'GB' THEN 3
					WHEN 'TB' THEN 4
					WHEN 'PB' THEN 5
					WHEN 'EB' THEN 6
					WHEN 'MISSING_UNIT' THEN 0
				END)) * ((regexp_split_to_array(meminfo.mir, '\s+'))[2])::BIGINT)::BIGINT AS mv
			FROM
				regexp_split_to_table(utilities.read_file_abs('/proc/meminfo', 0, 8388608), '\n') AS meminfo(mir)
			WHERE
				(meminfo.mir ~ '[0-9A-Za-z]')
		)
	SELECT
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'MEMTOTAL')) AS mem_total,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'MEMFREE')) AS mem_free,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'MEMAVAILABLE')) AS mem_available,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'BUFFERS')) AS buffers,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'CACHED')) AS cached,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SWAPCACHED')) AS swap_cached,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'ACTIVE')) AS active,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'INACTIVE')) AS inactive,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'ACTIVE(ANON)')) AS active_anon,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'INACTIVE(ANON)')) AS inactive_anon,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'ACTIVE(FILE)')) AS active_file,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'INACTIVE(FILE)')) AS inactive_file,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'UNEVICTABLE')) AS unevictable,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'MLOCKED')) AS m_locked,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SWAPTOTAL')) AS swap_total,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SWAPFREE')) AS swap_free,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'DIRTY')) AS dirty,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'WRITEBACK')) AS writeback,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'ANONPAGES')) AS anon_pages,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'MAPPED')) AS mapped,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SHMEM')) AS sh_mem,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SLAB')) AS slab,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SRECLAIMABLE')) AS s_reclaimable,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'SUNRECLAIM')) AS s_unreclaimable,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'KERNELSTACK')) AS kernel_stack,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'PAGETABLES')) AS page_tables,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'NFS_UNSTABLE')) AS nfs_unstable,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'BOUNCE')) AS bounce,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'WRITEBACKTMP')) AS writeback_tmp,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'COMMITLIMIT')) AS commit_limit,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'COMMITTED_AS')) AS committed_as,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'VMALLOCTOTAL')) AS vm_alloc_total,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'VMALLOCUSED')) AS vm_alloc_used,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'VMALLOCCHUNK')) AS vm_alloc_chunk,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HARDWARECORRUPTED')) AS hardware_corrupted,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'ANONHUGEPAGES')) AS anon_huge_pages,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'CMATOTAL')) AS cma_total,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'CMAFREE')) AS cma_free,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HUGEPAGES_TOTAL')) AS huge_pages_total,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HUGEPAGES_FREE')) AS huge_pages_free,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HUGEPAGES_RSVD')) AS huge_pages_rsvd,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HUGEPAGES_SURP')) AS huge_pages_surp,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'HUGEPAGESIZE')) AS huge_pages_size,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'DIRECTMAP4K')) AS direct_map_4k,
		(SELECT crs.mv FROM crs WHERE (crs.mk = 'DIRECTMAP2M')) AS direct_map_2m

$opm$;
COMMENT ON FUNCTION linux.get_proc_meminfo() IS 'SQL interface to /proc/meminfo.
Please note that that file info keys are subject to change between kernel versions. This means that columns may suddenly go "null" after kernel upgrades';
GRANT EXECUTE ON FUNCTION linux.get_proc_meminfo() TO PUBLIC;

CREATE VIEW linux.proc_meminfo AS
	SELECT * FROM linux.get_proc_meminfo();
;
COMMENT ON VIEW linux.proc_meminfo IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_meminfo TO PUBLIC;


CREATE FUNCTION linux.get_proc_filesystems() RETURNS TABLE (
	block_based BOOLEAN,
	fs_name TEXT
) LANGUAGE 'sql' STABLE SECURITY INVOKER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opm$
WITH
	fsrs AS (
		SELECT
			regexp_split_to_array(fsr, '\s+') AS rw
		FROM
			regexp_split_to_table(utilities.read_file_abs('/proc/filesystems', 0, 1048576), '\n') AS fss(fsr)
		WHERE
			(fss.fsr ~ '[0-9A-Za-z]')
	)
SELECT
	(upper(fsrs.rw[1]) != 'NODEV') AS "block_based",
	fsrs.rw[2] AS "fs_name"
FROM
	fsrs
;
$opm$;
COMMENT ON FUNCTION linux.get_proc_filesystems() IS '
Implementation of proc_filesystems. Written as a function to bypass security restrictions.
The columns are those in /proc/filesystem, with a catch: the "nodev" property is actually interpreted as "false"
as in "the file system requires a block device"
';
CREATE VIEW linux.proc_filesystems AS
	SELECT * FROM linux.get_proc_filesystems();
;
COMMENT ON VIEW linux.proc_filesystems IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_filesystems TO PUBLIC;



CREATE FUNCTION linux.get_proc_mounts() RETURNS TABLE (
	device TEXT,
	mount_point TEXT,
	fs_type TEXT,
	mount_options public.HSTORE,
	needs_dump BOOLEAN,
	fsck_order SMALLINT
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opm$
WITH
	mlw AS (
		SELECT
			regexp_split_to_array(fstab.ml, '\s+') AS w
		FROM
			regexp_split_to_table(utilities.read_file_abs('/proc/mounts', 0, 8388608), '\n') AS fstab(ml)
		WHERE
			(fstab.ml ~ '[0-9A-Za-z]')
	)
	SELECT
		m.w[1] AS device,
		m.w[2] AS mount_point,
		m.w[3] AS fs_type,
		(SELECT string_agg((CASE WHEN (mo.o ~ '=') THEN replace(mo.o, '=', '=>') ELSE CONCAT(mo.o, '=>null') END), ',') FROM regexp_split_to_table(m.w[4], ',') AS mo(o))::public.HSTORE AS mount_options,
		(m.w[5] != '0') AS needs_dump,
		m.w[6]::SMALLINT AS fsck_order
	FROM
		mlw AS m
$opm$;
COMMENT ON FUNCTION linux.get_proc_mounts() IS '
Implementation of os_proc_mounts. Written as a function to bypass security restrictions.
List of mounted volumes, as shown by /proc/mounts (documented in man fstab(5))
';
GRANT EXECUTE ON FUNCTION linux.get_proc_mounts() TO PUBLIC;

CREATE VIEW linux.proc_mounts AS
	SELECT * FROM linux.get_proc_mounts();
;
COMMENT ON VIEW linux.proc_mounts IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_mounts TO PUBLIC;


CREATE FUNCTION linux.get_stats_vfs(mps_list TEXT[] DEFAULT NULL) RETURNS TABLE (
	mount_point TEXT,
	block_size BIGINT,
	fragment_size BIGINT,
	fragments_total BIGINT,
	fragments_free BIGINT,
	fragments_available BIGINT,
	inodes_total BIGINT,
	inodes_free BIGINT,
	inodes_available BIGINT,
	mount_flags BIGINT,
	max_fn_lenght BIGINT
) LANGUAGE 'plpython3u' STABLE SECURITY INVOKER CALLED ON NULL INPUT COST 1000000000 AS $fss$
import os

sql_str = "SELECT mps.mount_point FROM linux.proc_mounts AS mps"
# is there a useful argument?
if (mps_list is not None):
	mps_no_nulls = filter(lambda nmp : (nmp is not None), mps_list)
	if (len(mps_no_nulls)):
		sql_str += " WHERE (mps.mount_point IN ("
		sql_str += ", ".join(plpy.quote_nullable(nmp) for nmp in mps_no_nulls)
		sql_str += "))"



for mount_point in map(lambda mp : mp["mount_point"], plpy.execute(sql_str)):
	# we skip the mount points for which it fails. With mounts there are so
	# many possible failure scenarios that we report it as mere debug noise

	# we reset the dictionary at every loop
	stat_out = {
		"mount_point": mount_point,
		"block_size": None, "fragment_size": None, "fragments_total": None, "fragments_free": None,
		"fragments_available": None, "inodes_total": None, "inodes_free": None,	"inodes_avail": None, "file_system_id": None,
		"mount_flags": None, "max_fn_lenght": None
	}
	mp_stat = None
	try:
		mp_stat = os.statvfs(mount_point)
		#plpy.info(mp_stat)
	except:
		plpy.debug("Unable to retrieve FS statistics for mount point '%s'" % (mount_point))
		continue;
	if (mp_stat is not None):
		stat_out["block_size"] = mp_stat.f_bsize
		stat_out["fragment_size"] = mp_stat.f_frsize
		stat_out["fragments_total"] = mp_stat.f_blocks
		stat_out["fragments_free"] = mp_stat.f_bfree
		stat_out["fragments_available"] = mp_stat.f_bavail
		stat_out["inodes_total"] = mp_stat.f_files
		stat_out["inodes_free"] = mp_stat.f_ffree
		stat_out["inodes_available"] = mp_stat.f_favail
		# f_fsid is not available in the python implementation? weird
		stat_out["mount_flags"] = mp_stat.f_flag
		stat_out["max_fn_lenght"] = mp_stat.f_namemax

	yield stat_out

return

$fss$;
COMMENT ON FUNCTION linux.get_stats_vfs(TEXT[]) IS '
Implementation of stats_vfs. Overlays information coming from statvfs(3) to the list coming from /proc/mounts 

Largely exists as a batch operation for optimization reasons (as plperl starts a new interpreter for each function call)


Arguments:
	an array of mount points we want to retrieve the information for.


If the argument is not passed, information is retrieved for all file systems
';
GRANT EXECUTE ON FUNCTION linux.get_stats_vfs(TEXT[]) TO PUBLIC;

CREATE VIEW linux.stats_vfs AS
	SELECT * FROM linux.get_stats_vfs();
;
COMMENT ON VIEW linux.stats_vfs IS 'See invoked function';
GRANT SELECT ON TABLE linux.stats_vfs TO PUBLIC;

CREATE VIEW linux.fss_details AS
	SELECT
		m.device,
		m.mount_point,
		m.fs_type,
		m.mount_options,
		m.needs_dump,
		m.fsck_order,
		s.fragment_size,
		s.fragments_total,
		s.fragments_free,
		s.fragments_available,
		s.inodes_total,
		s.inodes_free,
		s.inodes_available,
		(CASE
			WHEN (fst.block_based) THEN
				COALESCE(1.0 - (s.fragments_available::FLOAT / NULLIF(s.fragments_total, 0)), 0.0)
			ELSE
				0.0
		END) AS fraction_used
	FROM
		(
			linux.proc_filesystems AS fst INNER JOIN linux.proc_mounts AS m
			ON
				(m.fs_type = fst.fs_name)
		) LEFT JOIN linux.stats_vfs AS s
		ON
			(s.mount_point = m.mount_point)
;
COMMENT ON VIEW linux.stats_vfs IS 'Shorthand for aggregated volume info. Note that fraction_free is reported as 1 for volumes that don''t have a stated block number (presumably pseudo-fss)';
GRANT SELECT ON TABLE linux.fss_details TO PUBLIC;



CREATE FUNCTION linux.get_proc_stat_cpus() RETURNS TABLE (
	cpu_id SMALLINT,
	jiffies_user BIGINT,
	jiffies_nice BIGINT,
	jiffies_system BIGINT,
	jiffies_idle BIGINT,
	jiffies_iowait BIGINT,
	jiffies_irq BIGINT,
	jiffies_softirq BIGINT,
	jiffies_steal BIGINT,
	jiffies_guest BIGINT
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $ops$
	-- it is very important that we use CTEs here as the functions need to be materialized
	WITH
		cpu_stat AS (
				SELECT
					regexp_split_to_array(regexp_replace(UPPER(st.row_text), '^CPU', ''), '\s+') AS cpu_fields
				FROM
					regexp_split_to_table(utilities.read_file_abs('/proc/stat', 0, 1048576), '\n', 'n') AS st(row_text)
				WHERE
					UPPER(st.row_text) ~ '^CPU[0-9]*\s'
		)
	SELECT
		CAST((CASE WHEN (cpu_stat.cpu_fields[1] ~ '^[0-9]+$') THEN cpu_stat.cpu_fields[1] ELSE null END) AS SMALLINT) AS cpu_id,
		CAST(cpu_stat.cpu_fields[2] AS BIGINT) AS jiffies_user,
		CAST(cpu_stat.cpu_fields[3] AS BIGINT) AS jiffies_nice,
		CAST(cpu_stat.cpu_fields[4] AS BIGINT) AS jiffies_system,
		CAST(cpu_stat.cpu_fields[5] AS BIGINT) AS jiffies_idle,
		CAST(cpu_stat.cpu_fields[6] AS BIGINT) AS jiffies_iowait,
		CAST(cpu_stat.cpu_fields[7] AS BIGINT) AS jiffies_irq,
		CAST(cpu_stat.cpu_fields[8] AS BIGINT) AS jiffies_softirq,
		CAST(cpu_stat.cpu_fields[9] AS BIGINT) AS jiffies_steal,
		CAST(cpu_stat.cpu_fields[10] AS BIGINT) AS jiffies_guest
	FROM
		cpu_stat
$ops$;
COMMENT ON FUNCTION linux.get_proc_stat_cpus() IS '
Implementation of os_proc_stat. Written as a function to bypass security restrictions.
A relalational friendly rendering of /proc/stat, FOR CPU ACTIVITY INFO ONLY!!!
Note that an NULL cpu_id represents the grand total
';
GRANT EXECUTE ON FUNCTION linux.get_proc_stat_cpus() TO PUBLIC;


CREATE VIEW linux.proc_stat_cpus AS
	SELECT * FROM linux.get_proc_stat_cpus();
;
COMMENT ON VIEW linux.proc_stat_cpus IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_stat_cpus TO PUBLIC;

CREATE FUNCTION linux.get_proc_loadavg() RETURNS TABLE (
	loadavg_60s FLOAT,
	loadavg_300s FLOAT,
	loadavg_900s FLOAT,
	tasks_runnable BIGINT,
	tasks_total BIGINT,
	last_pid BIGINT
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opl$
	WITH
		loadavg AS (
			SELECT
				regexp_split_to_array(regexp_replace(utilities.read_file_abs('/proc/loadavg', 0, 65536), '\n|\r', ''), '\s+') AS f
		)
	SELECT
		CAST(l.f[1] AS DOUBLE PRECISION) AS loadavg_60s,
		CAST(l.f[2] AS DOUBLE PRECISION) AS loadavg_300s,
		CAST(l.f[3] AS DOUBLE PRECISION) AS loadavg_900s,
		CAST(split_part(l.f[4], '/', 1) AS BIGINT) AS tasks_runnable,
		CAST(split_part(l.f[4], '/', 2) AS BIGINT) AS tasks_total,
		CAST(l.f[5] AS BIGINT) AS last_pid
	FROM
		loadavg AS l

$opl$;
COMMENT ON FUNCTION linux.get_proc_loadavg() IS '
Implementation of os_proc_loadavg. Written as a function to bypass security restrictions.
A relalational friendly rendering of /proc/loadavg
';
GRANT EXECUTE ON FUNCTION linux.get_proc_loadavg() TO PUBLIC;

CREATE VIEW "linux"."proc_loadavg" AS
	SELECT * FROM linux.get_proc_loadavg();
;
COMMENT ON VIEW "linux"."proc_loadavg" IS 'See invoked function';
GRANT SELECT ON TABLE "linux"."proc_loadavg" TO PUBLIC;



CREATE FUNCTION linux.get_proc_processes_stat() RETURNS TABLE (
	pid BIGINT,
	comm VARCHAR(256),
	"state" CHAR(1),
	ppid BIGINT,
	pgrp BIGINT,
	"session" BIGINT,
	tty_nr BIGINT,
	tpgid BIGINT,
	flags BIGINT,
	minflt NUMERIC(20, 0),
	cminflt NUMERIC(20, 0),
	majflt NUMERIC(20, 0),
	cmajflt NUMERIC(20, 0),
	utime NUMERIC(20, 0),
	stime NUMERIC(20, 0),
	cutime NUMERIC(20, 0),
	cstime NUMERIC(20, 0),
	priority NUMERIC(20, 0),
	nice NUMERIC(20, 0),
	num_threads NUMERIC(20, 0),
	itrealvalue NUMERIC(20, 0),
	starttime NUMERIC(20, 0),
	vsize NUMERIC(20, 0),
	rss NUMERIC(20, 0),
	rsslim NUMERIC(20, 0),
	startcode NUMERIC(20, 0),
	endcode NUMERIC(20, 0),
	startstack NUMERIC(20, 0),
	kstkesp NUMERIC(20, 0),
	kstkeip NUMERIC(20, 0),
	signal NUMERIC(20, 0),
	blocked NUMERIC(20, 0),
	sigignore NUMERIC(20, 0),
	sigcatch NUMERIC(20, 0),
	wchan NUMERIC(20, 0),
	nswap NUMERIC(20, 0),
	cnswap NUMERIC(20, 0),
	exit_signal NUMERIC(20, 0),
	processor NUMERIC(20, 0),
	rt_priority NUMERIC(20, 0),
	policy NUMERIC(20, 0),
	delayacct_blkio_ticks NUMERIC(20, 0),
	guest_time NUMERIC(20, 0),
	cguest_time NUMERIC(20, 0)
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opp$
	-- it is very important that we use CTEs here as the perl functions need to be materialized
	WITH
		p AS (
			SELECT
				-- some retarded process injects white spaces into the process image name (I'm looking at you, EMC HBAs driver).
				-- This makes separating /proc/<pid>/stat a fields a little tricky. We rely on brackets first
				(
					ARRAY[
						stat_files.image_split[1], -- PID
						stat_files.image_split[2]  -- argv0
					]
				||
					STRING_TO_ARRAY(stat_files.image_split[3], ' ') -- everything else
				) AS proc_stat_data

			FROM
				-- we use our multi-file open function
				(
					SELECT
						regexp_split_to_array(REPLACE(rf.fd, CHR(10), ''), '\s+\(|\)\s+') AS image_split
					FROM
						utilities.read_files_abs(
							(SELECT array_agg(CONCAT('/proc/', pd.proc_dir, '/stat')) FROM utilities.ls_dir_abs('/proc') AS pd(proc_dir) WHERE (pd.proc_dir ~ '^[0-9]{1,14}$')),
							0,
							65536,
							true
						) AS rf
				) AS stat_files
		)
	SELECT
		CAST(p.proc_stat_data[1] AS BIGINT) AS pid,
		CAST(p.proc_stat_data[2] AS VARCHAR(256)) AS comm,
		CAST(p.proc_stat_data[3] AS CHAR(1)) AS "state",
		CAST(p.proc_stat_data[4] AS BIGINT) AS ppid,
		CAST(p.proc_stat_data[5] AS BIGINT) AS pgrp,
		CAST(p.proc_stat_data[6] AS BIGINT) AS "session",
		CAST(p.proc_stat_data[7] AS BIGINT) AS tty_nr,
		CAST(p.proc_stat_data[8] AS BIGINT) AS tpgid,
		CAST(p.proc_stat_data[9] AS BIGINT) AS flags,
		CAST(p.proc_stat_data[10] AS NUMERIC(20, 0)) AS minflt,
		CAST(p.proc_stat_data[11] AS NUMERIC(20, 0)) AS cminflt,
		CAST(p.proc_stat_data[12] AS NUMERIC(20, 0)) AS majflt,
		CAST(p.proc_stat_data[13] AS NUMERIC(20, 0)) AS cmajflt,
		CAST(p.proc_stat_data[14] AS NUMERIC(20, 0)) AS utime,
		CAST(p.proc_stat_data[15] AS NUMERIC(20, 0)) AS stime,
		CAST(p.proc_stat_data[16] AS NUMERIC(20, 0)) AS cutime,
		CAST(p.proc_stat_data[17] AS NUMERIC(20, 0)) AS cstime,
		CAST(p.proc_stat_data[18] AS NUMERIC(20, 0)) AS priority,
		CAST(p.proc_stat_data[19] AS NUMERIC(20, 0)) AS nice,
		CAST(p.proc_stat_data[20] AS NUMERIC(20, 0)) AS num_threads,
		CAST(p.proc_stat_data[21] AS NUMERIC(20, 0)) AS itrealvalue,
		CAST(p.proc_stat_data[22] AS NUMERIC(20, 0)) AS starttime,
		CAST(p.proc_stat_data[23] AS NUMERIC(20, 0)) AS vsize,
		CAST(p.proc_stat_data[24] AS NUMERIC(20, 0)) AS rss,
		CAST(p.proc_stat_data[25] AS NUMERIC(20, 0)) AS rsslim,
		CAST(p.proc_stat_data[26] AS NUMERIC(20, 0)) AS startcode,
		CAST(p.proc_stat_data[27] AS NUMERIC(20, 0)) AS endcode,
		CAST(p.proc_stat_data[28] AS NUMERIC(20, 0)) AS startstack,
		CAST(p.proc_stat_data[29] AS NUMERIC(20, 0)) AS kstkesp,
		CAST(p.proc_stat_data[30] AS NUMERIC(20, 0)) AS kstkeip,
		CAST(p.proc_stat_data[31] AS NUMERIC(20, 0)) AS signal,
		CAST(p.proc_stat_data[32] AS NUMERIC(20, 0)) AS blocked,
		CAST(p.proc_stat_data[33] AS NUMERIC(20, 0)) AS sigignore,
		CAST(p.proc_stat_data[34] AS NUMERIC(20, 0)) AS sigcatch,
		CAST(p.proc_stat_data[35] AS NUMERIC(20, 0)) AS wchan,
		CAST(p.proc_stat_data[36] AS NUMERIC(20, 0)) AS nswap,
		CAST(p.proc_stat_data[37] AS NUMERIC(20, 0)) AS cnswap,
		CAST(p.proc_stat_data[38] AS NUMERIC(20, 0)) AS exit_signal,
		CAST(p.proc_stat_data[39] AS NUMERIC(20, 0)) AS processor,
		CAST(p.proc_stat_data[40] AS NUMERIC(20, 0)) AS rt_priority,
		CAST(p.proc_stat_data[41] AS NUMERIC(20, 0)) AS policy,
		CAST(p.proc_stat_data[42] AS NUMERIC(20, 0)) AS delayacct_blkio_ticks,
		CAST(p.proc_stat_data[43] AS NUMERIC(20, 0)) AS guest_time,
		CAST(p.proc_stat_data[44] AS NUMERIC(20, 0)) AS cguest_time
	FROM
		p
	;
$opp$;
COMMENT ON FUNCTION linux.get_proc_processes_stat() IS '
Implementation of os_proc_processes. Written as a function to bypass security restrictions.
basically the contents of /proc/<pid>/stat, in a relational-friendly way.
Unfortunately many of the exported values are numeric representation of 64 bit unsigned longs,
which force us to use numeric (the alternative would be altering the actual value by casting them to signed)
';
GRANT EXECUTE ON FUNCTION linux.get_proc_processes_stat() TO PUBLIC;


CREATE VIEW linux.proc_processes_stat AS
	SELECT * FROM linux.get_proc_processes_stat()
;
COMMENT ON VIEW linux.proc_processes_stat IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_processes_stat TO PUBLIC;


CREATE FUNCTION linux.get_proc_processes_cmdline() RETURNS TABLE (
	pid BIGINT,
	argv0 TEXT,
	argv TEXT[]
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $opcmd$
	WITH
		ia AS (
			SELECT
				split_part(cf.fn, '/', 3)::BIGINT AS pid,
				regexp_split_to_array(regexp_replace(encode(cf.fd, 'escape'), '\\000$', ''), '\\000') AS argv
			FROM
				utilities.read_binary_files_abs(
					(SELECT array_agg(CONCAT('/proc/', pd.proc_dir, '/cmdline')) FROM utilities.ls_dir_abs('/proc') AS pd(proc_dir) WHERE (pd.proc_dir ~ '^[0-9]{1,14}$')),
					0,
					65536,
					true
				) AS cf
		)
	SELECT
		ia.pid,
		ia.argv[1] AS argv0,
		ia.argv
	FROM
		ia
	;
$opcmd$;


COMMENT ON FUNCTION linux.get_proc_processes_cmdline() IS '
Implementation of os_proc_cmdlines. Written as a function to bypass security restrictions.
basically the contents of /proc/<pid>/cmdline, in a relational-friendly way.
Columns:
	pid:     true to namesake
	argv0:   true to namesake
	argv:    array of all the arguments, including argv0
';


CREATE VIEW linux.proc_processes_cmdline AS
	SELECT * FROM linux.get_proc_processes_cmdline()
;
COMMENT ON VIEW linux.proc_processes_cmdline IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_processes_cmdline TO PUBLIC;


CREATE FUNCTION linux.get_proc_processes_io() RETURNS TABLE (
	pid BIGINT,
	rchar NUMERIC(20, 0),
	wchar NUMERIC(20, 0),
	syscr NUMERIC(20, 0),
	syscw NUMERIC(20, 0),
	read_bytes NUMERIC(20, 0),
	write_bytes NUMERIC(20, 0),
	cancelled_write_bytes NUMERIC(20, 0)
) LANGUAGE 'sql' STABLE SECURITY DEFINER RETURNS NULL ON NULL INPUT COST 1000000000 AS $oppio$
	-- it is very important that we use CTEs here as the perl functions need to be materialized
	WITH
		osp AS (
			SELECT
				split_part(rf.fn, '/', 3)::BIGINT AS proc_pid,
				rf.fd AS proc_stat_data
			FROM
				utilities.read_files_abs(
					(SELECT array_agg(CONCAT('/proc/', pd.item_name, '/io')) FROM utilities.ls_dir_abs('/proc') AS pd(item_name) WHERE (pd.item_name ~ '^[0-9]+$')),
					0,
					65536,
					true
				) AS rf
		)
	SELECT
		CAST(osp.proc_pid AS BIGINT) AS pid,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)rchar: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS rchar,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)wchar: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS wchar,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)syscr: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS syscr,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)syscw: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS syscw,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)read_bytes: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS read_bytes,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)write_bytes: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS write_bytes,
		CAST(regexp_replace(regexp_replace(osp.proc_stat_data, '.*(\n|^)cancelled_write_bytes: *', '', ''), '[^0-9].*', '') AS NUMERIC(20, 0)) AS cancelled_write_bytes
	FROM
		osp
$oppio$;
COMMENT ON FUNCTION linux.get_proc_processes_io() IS '
Implementation of os_proc_processes_io. Written as a function to bypass security restrictions.
basically the contents of /proc/<pid>/io, in a relational-friendly way. It returns usable info about postgresql backend processes only (due to /proc/<pid>/io permissions).
It uses numeric to prevent signed int overflow from unsigned long input :(
';
GRANT EXECUTE ON FUNCTION linux.get_proc_processes_io() TO PUBLIC;

CREATE VIEW linux.proc_processes_io AS
	SELECT * FROM linux.get_proc_processes_io()
;
COMMENT ON VIEW linux.proc_processes_io IS 'See invoked function';
GRANT SELECT ON TABLE linux.proc_processes_io TO PUBLIC;




-- 9.6 introduced a breaking change to pg_stat_activity. Need plpgsql trickery
DO LANGUAGE 'plpgsql' $ss$
DECLARE sql_str TEXT;
BEGIN

	sql_str = '(p.wait_event_type IS NOT NULL)';
	IF ((SELECT setting::BIGINT FROM pg_settings WHERE "name" = 'server_version_num') < 90600) THEN
		sql_str = 'p.waiting';
	END IF;
		sql_str = CONCAT(
			$the_sql$CREATE VIEW instrumentation.sessions_status AS
					SELECT
						p.pid AS session_pid,
						COALESCE(NULLIF(TRIM(p.client_hostname), ''), host(p.client_addr)) AS client_host,
						p.client_port AS client_port,
						ps."state" AS process_state,
						ps.utime AS process_utime,
						ps.stime AS process_stime,
						pio.rchar AS process_rchar,
						pio.wchar AS process_wchar,
						usename AS user_name,
						datname AS database_name,
						application_name AS app_name,
						CAST((EXTRACT('epoch' FROM (clock_timestamp() - p.backend_start)) * 1000.0) AS BIGINT) AS process_age,
						CAST((CASE WHEN (UPPER(p.state) ~ '((ACTIVE)|(FASTPATH)|(IN TRANSACTION))') THEN (EXTRACT('epoch' FROM (clock_timestamp() - p.query_start)) * 1000.0) ELSE -1.0 END) AS BIGINT) AS statement_age,
						(CASE WHEN $the_sql$,
			sql_str,
			$more_sql$ THEN gl.pid ELSE null END) AS blocking_lock_pid,
					(
						SELECT
							wlr.relation::REGCLASS
						FROM
							pg_locks AS wlr
						WHERE
							wlr.pid = p.pid
						AND
							(UPPER(wlr.locktype) IN ('RELATION', 'TUPLE'))
						AND
							(wlr.virtualtransaction = wl.virtualtransaction)
						ORDER BY
							(wlr.mode = 'AccessExclusiveLock') DESC
						LIMIT 1
					) AS lock_relation,
					p.query AS query_sql
				FROM
					(
						(
							(
								pg_stat_activity AS p INNER JOIN linux.proc_processes_stat AS ps
								ON
									(ps.pid = p.pid)
							) INNER JOIN linux.proc_processes_io AS pio
							ON
								(pio.pid = p.pid)
						) LEFT JOIN pg_locks AS wl
						ON
							(wl.pid = p.pid)
						AND
							(NOT wl.granted)
					) LEFT JOIN pg_locks AS gl
					ON
						gl.granted
					AND
						(gl.pid != wl.pid)
					AND
						(gl."database" IS NOT DISTINCT FROM wl."database")
					AND
						(gl.relation IS NOT DISTINCT FROM wl.relation)
					AND
						(gl.page IS NOT DISTINCT FROM wl.page)
					AND
						(gl.tuple IS NOT DISTINCT FROM wl.tuple)
					AND
						(gl.virtualxid IS NOT DISTINCT FROM wl.virtualxid)
					AND
						(gl.transactionid IS NOT DISTINCT FROM wl.transactionid)
					AND
						(gl.classid IS NOT DISTINCT FROM wl.classid)
					AND
						(gl.objid IS NOT DISTINCT FROM wl.objid)
					AND
						(gl.objsubid IS NOT DISTINCT FROM wl.objsubid)
			$more_sql$
		);

	--RAISE INFO '%s', sql_str;
	EXECUTE sql_str;

END;
$ss$;

COMMENT ON VIEW instrumentation.sessions_status IS 'pg_stat_activity on steroids. It shows the locking process/relation and retrieves additional information from the OS (from /proc).
9.6 Has introduced some breaking changes to how locks are exposed but this view does not use the new model yet
';


CREATE VIEW instrumentation.replication_streams_status AS
	SELECT

		rp.pid AS wal_sender_pid,
		COALESCE(NULLIF(TRIM(rp.client_hostname), ''), host(rp.client_addr)) AS client_host,
		rp.client_port AS client_port,
		ps."state" AS process_state,
		ps.utime AS process_utime,
		ps.stime AS process_stime,
		pio.rchar AS process_rchar,
		pio.wchar AS process_wchar,
		rp.usename AS user_name,
		rp.application_name AS application_name,
		CAST((EXTRACT('epoch' FROM (clock_timestamp() - rp.backend_start)) * 1000.0) AS BIGINT) AS process_age,
		rp."state" AS state_str,
		rp.sent_lsn,
		pg_wal_lsn_diff(rp.sent_lsn, '000/00000000') AS sent_lsn_bytes,
		rp.write_lsn,
		pg_wal_lsn_diff(rp.write_lsn, '000/00000000') AS write_lsn_bytes,
		rp.replay_lsn,
		pg_wal_lsn_diff(rp.replay_lsn, '000/00000000') AS replay_lsn_bytes,
		rp.sync_priority,
		rp.sync_state

	FROM
		(
			(
				pg_stat_replication AS rp INNER JOIN linux.proc_processes_stat AS ps
				ON
					(ps.pid = rp.pid)
			) INNER JOIN linux.proc_processes_io AS pio
			ON
				(pio.pid = rp.pid)
		)
;
COMMENT ON VIEW instrumentation.replication_streams_status IS 'Much like sessions_status does for pg_stat_activity, this is enriches pg_stat_replication with info from the os';




CREATE FUNCTION instrumentation.get_postmaster_pid() RETURNS BIGINT LANGUAGE 'sql' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
SELECT split_part(pg_read_file('./postmaster.pid'), E'\n', 1)::BIGINT;
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION instrumentation.get_postmaster_pid() FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION instrumentation.get_postmaster_pid() IS 'Retrieves the PID of the postmaster';



CREATE FUNCTION instrumentation.get_wal_receiver_pid() RETURNS BIGINT LANGUAGE 'sql' SECURITY INVOKER STABLE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
SELECT
	cmd.pid	AS wal_receiver_pid
FROM
	linux.proc_processes_stat AS op INNER JOIN linux.proc_processes_cmdline AS cmd
ON
	(op.ppid = instrumentation.get_postmaster_pid())
AND
	(cmd.pid = op.pid)
AND
	(cmd.argv0 ~* 'WAL\s+RECEIVER')
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION instrumentation.get_wal_receiver_pid() FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION instrumentation.get_wal_receiver_pid() IS 'Retrieves the PID of the stream replication client.
Unfortunately there is no builtin mechanism that allows to do it reliably, so we have to look for child processes of the postmaster that somewhat match the correct argv0.
Wonky';




CREATE FUNCTION administration.restart_wal_receiver() RETURNS BIGINT LANGUAGE 'plpython3u' SECURITY INVOKER VOLATILE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
import os
import signal

pr_pid = plpy.execute("SELECT instrumentation.get_wal_receiver_pid() AS wrp")[0]["wrp"]

if (pr_pid is not None):
	pr_pid = int(pr_pid)
	os.kill(pr_pid, signal.SIGTERM)
return pr_pid

$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION administration.restart_wal_receiver() FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION administration.restart_wal_receiver() IS 'As the name implies, sends a termination signal to the process identified as the WAL receiver.
Unfortunately only 9.6+ has builtin SQL to retrieve its run-time status, so we have to make do with a naive process name approach.

Returns the pid of the old WAL receiver that was running, null otherwise.
Does NOT fail fatally on machines that are not in recovery. Just returns null as there is no wal receiver

Raises a fatal exception only on signal delivery failure
';


CREATE FUNCTION administration.promote_postmaster() RETURNS BOOLEAN LANGUAGE 'plpython3u' SECURITY INVOKER VOLATILE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
import os
import signal

if (not plpy.execute("SELECT pg_is_in_recovery() AS r")[0]["r"]):
	plpy.error("This postmaster is not in recovery. Cannot promote")

# the cast is to make it reliably fail
pm_pid = int(plpy.execute("SELECT instrumentation.get_postmaster_pid() AS pmpid")[0]["pmpid"])

pf = open("./promote", "w")
pf.close()
os.kill(pm_pid, signal.SIGUSR1)

return True # a legacy of when the function was perl and call errors were not fatal
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION administration.promote_postmaster() FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION administration.promote_postmaster() IS 'SQL version of pg_ctl promote. Use with caution.
Inspired by http://doxygen.postgresql.org/pg__ctl_8c_source.html';



CREATE FUNCTION administration.nuke_postmaster() RETURNS BOOLEAN LANGUAGE 'plpython3u' SECURITY INVOKER VOLATILE RETURNS NULL ON NULL INPUT COST 1000000000 AS $CODE$
import os
import signal

# the cast is to make it reliably fail
pm_pid = int(plpy.execute("SELECT instrumentation.get_postmaster_pid() AS pmpid")[0]["pmpid"])

os.rename("./postgresql.conf", ("./postgresql.conf.nuked_by_%d" % os.getpid()))

os.kill(pm_pid, signal.SIGQUIT)

return True
$CODE$;
REVOKE ALL PRIVILEGES ON FUNCTION administration.nuke_postmaster() FROM PUBLIC; -- just for good measure
COMMENT ON FUNCTION administration.nuke_postmaster() IS 'WARNING!!! This function will purposely render the data cluster unusable until the configuration file is renamed back.
It is meant to be used as an additional safety measure by failover processes in case of a zombie master';


COMMIT TRANSACTION;
